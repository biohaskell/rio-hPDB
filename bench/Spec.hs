import Bio.PDB
import Criterion.Main
import RIO
import RIO.Directory
import RIO.FilePath

listDirectoryQualified = ((<$>) . fmap . (</>)) <*> listDirectory

main = do
  data_res <- listDirectoryQualified "data/residues"
  defaultMain [
     bgroup "parseResidueFiles" $
        data_res <&> bench <*> whnfIO . parse
     ]
