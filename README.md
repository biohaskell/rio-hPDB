rio-hPDB
========
Haskell PDB file format parser, based on the RIO prelude.

Orginally forked from the hPDB file format parser on [github](https://github.com/BioHaskell/hPDB)
