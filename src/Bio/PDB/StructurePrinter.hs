{-# LANGUAGE DisambiguateRecordFields #-}
-- | High-level output routines for 'Structure'.
module Bio.PDB.StructurePrinter(write, PDBWritable()) where

import RIO
import System.IO(Handle)
import Control.Lens

import Bio.PDB.Structure
import qualified Bio.PDB.Structure.List as L
import Bio.PDB.EventParser.PDBEventPrinter as PR
import Bio.PDB.EventParser.PDBEvents 

-- | ShowS like type for a list of `PDBEvent`s.
type PDBEventS = [PDBEvent] -> [PDBEvent]

-- * Class-based interface for generating PDB events from structure fragments.
-- | Writes a structure or its part in a PDB format to a filehandle.
write :: PDBWritable a => Handle -> a -> IO ()
write handle structure = mapM_ (PR.print handle) (pdbEvents structure)

-- | Class generating events for PDB structure fragments.
class PDBWritable a where
    pdbEvents :: a -> [PDBEvent]
    pdbEvents = flip pdbEventS []
    pdbEventS :: a -> PDBEventS

instance PDBWritable Structure
  where
    pdbEvents = structureEvents
    pdbEventS = error "Structure is closed by definition cannot have continuation!"

instance PDBWritable Model
  where
    pdbEventS = modelEvents

instance PDBWritable Chain
  where
    pdbEventS = chainEvents

instance PDBWritable Residue
  where
    pdbEventS = residueEvents blankChain

instance PDBWritable Atom
  where
    pdbEventS = atomEvents blankChain blankResidue

-- | Helper: blank chain in case we don't know which chain residue belongs to.
blankChain   :: Chain
blankChain   = Chain { _chainId  = ' '
                     , _residues = L.empty }

-- | Helper blank residue in case we don't know which residue the atom belongs to.
blankResidue :: Residue
blankResidue = Residue { _resName = "UNK"
                       , _resSeq  = 0
                       , _insCode = ' '
                       , _atoms   = L.empty }

-- * Routines for writing event list for fragments of the structure.
-- | Generates list of `PDBEvent`s from a given Structure.
structureEvents :: Structure -> [PDBEvent]
structureEvents x = foldr modelEvents [END] (x ^.. models . traverse)

-- | Generates list of `PDBEvent`s from a given Model.
modelEvents :: Model -> PDBEventS
modelEvents m cont = start:main (ENDMDL : cont)
  where
    start   = MODEL $ _modelId m
    main  c = foldr chainEvents c (m ^.. chains . traverse)

-- | Generates list of `PDBEvent`s from a given Chain.
chainEvents :: Chain -> PDBEventS
chainEvents ch c = foldr (residueEvents ch) (ter:c) (ch ^.. Bio.PDB.Structure.residues . traverse)
  where
    ter = TER { num     = atSer + 1      , -- FIXME: should be lastAtom ch + 1
                resname = lastResName    ,
                chain   = _chainId ch     ,
                resid   = lastResSeq     ,
                insCode = lastInsCode    }
    Atom    { _atSerial = atSer } = L.last ats
    Residue { _resName = lastResName,
              _resSeq  = lastResSeq ,
              _insCode = lastInsCode,
              _atoms   = ats
            } = L.last . _residues $ ch

-- | Generates list of `PDBEvent`s from a given Residue and its Chain.
residueEvents :: Chain -> Residue -> PDBEventS
residueEvents ch r c = foldr (atomEvents ch r) c (r ^.. Bio.PDB.Structure.atoms . traverse)

-- | Generates list of `PDBEvent`s from a given Atom, its Residue, and its Chain.
atomEvents :: Chain -> Residue -> Atom -> PDBEventS
atomEvents    Chain { _chainId = chid }
              Residue { _resName = rtype,
                        _resSeq  = rid,
                        _insCode = rins
                      }
              Atom { _atName    = atName,
                     _atSerial  = atSer,
                     _coord     = coord,
                     _bFactor   = bf,
                     _occupancy = occ,
                     _element   = e,
                     _segid     = sid,
                     _charge    = ch,
                     _hetatm    = isHet
                   } c = ATOM { no        = atSer, -- TODO: assign atom serial numbers
                                 atomtype  = atName,
                                 restype   = rtype,
                                 chain     = chid,
                                 resid     = rid,
                                 resins    = rins,
                                 altloc    = ' ',
                                 coords    = coord,
                                 occupancy = occ,
                                 bfactor   = bf,
                                 segid     = sid,
                                 elt       = e,
                                 charge    = ch,
                                 hetatm    = isHet
                               } : c

