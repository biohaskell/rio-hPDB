-- | Basic parallel folding utility.
module Bio.PDB.Util.ParFold(parFold1) where

import RIO
import Control.Parallel.Strategies(parList, rseq, using)

-- | Parallel folding like fold1, but assuming associativity and using O(n*lg)
parFold1 f [ ] = error "parFold of empty list!"
parFold1 f [a] = a
parFold1 f l   = parFold1 f . (`using` parList rseq) $ aList
  where
    aList             = foldLevel l
    -- | reduction by one level
    foldLevel [ ]     = [ ]
    foldLevel [a]     = [a]
    foldLevel (a:b:l) = (a `f` b):foldLevel l
