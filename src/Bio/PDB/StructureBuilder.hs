-- | Front-end module presenting minimal interface for serial and parallel parsing.
module Bio.PDB.StructureBuilder(parse
                               ,parseSerial
                               ,parseParallel
                               ,parseWithNParallel)
where

import RIO
import Bio.PDB.StructureBuilder.Internals
import Bio.PDB.StructureBuilder.Parallel
-- For type declaration:
import qualified Data.ByteString.Char8 as BS
import Bio.PDB.Structure.List(List(..))
import Bio.PDB.Structure(Structure)
import Bio.PDB.EventParser.PDBEvents(PDBEvent)

-- | Default parser - uses parallel capabilities, if available.
parse :: FilePath -> BS.ByteString -> (Structure,
                                List PDBEvent)
parse = parseParallel
