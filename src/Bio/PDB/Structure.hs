{-# LANGUAGE DisambiguateRecordFields #-}
{-# LANGUAGE TemplateHaskell #-}
-- | Module defines all components of high-level data type description of PDB model.
module Bio.PDB.Structure(String,
                         vdot, vnorm, vproj, vperpend, vperpends, vdihedral, (*|), (|*), models,
                         modelId, chains, chainId, residues, resName, resSeq, atoms, insCode,
                         atName, atSerial, coord, bFactor, occupancy, element, segid, charge, hetatm,
                         Structure(..), Model(..), Chain(..), Residue(..), Atom(..))

where

import Lens.Micro.Platform
import RIO
import qualified Data.ByteString.Char8 as BS
import Bio.PDB.Structure.List as L
import Bio.PDB.Structure.Vector


-- | Structure holds all data parsed from a single PDB entry
newtype Structure = Structure { _models    :: L.List Model } deriving (Eq, Show, Generic)

instance NFData Structure
instance NFData Model
instance NFData Chain
instance NFData Residue
instance NFData Atom

-- | PDB entry may contain multiple models, with slight differences in coordinates etc.
data Model     = Model     { _modelId   :: !Int,
                             _chains    :: L.List Chain
                           } deriving (Eq, Show, Generic)


-- | Single linear polymer chain of protein, or nucleic acids
data Chain     = Chain     { _chainId   :: !Char,
                             _residues  :: L.List Residue
                           } deriving (Eq, Show, Generic)

-- | Residue groups all atoms assigned to the same aminoacid or nucleic acid base within a polymer chain.
data Residue   = Residue   { _resName   :: !BS.ByteString,
                             _resSeq    :: !Int,
                             _atoms     :: L.List Atom,
                             _insCode   :: !Char
                           } deriving (Eq, Show, Generic)

-- | Single atom position
-- | NOTE: disordered atoms are now reported as multiplicates
data Atom      = Atom      { _atName    :: !BS.ByteString,
                             _atSerial  :: !Int,
                             _coord     :: !(V3 Double),

                             _bFactor   :: !Double,
                             _occupancy :: !Double,
                             _element   :: !BS.ByteString,
                             _segid     :: !BS.ByteString,
                             _charge    :: !BS.ByteString,
                             _hetatm    :: !Bool
                           } deriving (Eq, Show, Generic)

$(makeLenses ''Structure)
$(makeLenses ''Residue)
$(makeLenses ''Atom)
$(makeLenses ''Chain)
$(makeLenses ''Model)
