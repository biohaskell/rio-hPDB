# Changelog for rio-hPDB

## v0.0.1.0

* Fork from hPDB (http://hackage.haskell.org/package/hPDB)
* Add .gitlab-ci.yml using zenhaskell (http://zenhaskell.gitlab.io)
* Change maintainers information
* Clarify String/ByteString usage
* Add lenses to data structure
